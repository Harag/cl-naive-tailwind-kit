(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-radio
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-radio-label ,(lts "text-blue-500"
                                                      "dark:text-zinc-400")

                               :oic-outer ,(lts "text-blue-500"
                                                "focus-within:ring-blue-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))
                              (:name :outline
                               :oic-radio-label ,(lts "text-blue-500"
                                                      "dark:text-zinc-400")
                               :oic-outer ,(lts "text-blue-500"
                                                "border"
                                                "border-blue-400"
                                                "focus-within:ring-blue-400"
                                                "dark:border-zinc-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))))
                     (:name "secondary"
                      :types ((:name :default
                               :oic-radio-label ,(lts
                                                  "text-zinc-500"

                                                  "dark:text-zinc-400")

                               :oic-outer ,(lts "text-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500"))
                              (:name :outline
                               :oic-radio-label ,(lts
                                                  "text-zinc-500"
                                                  "dark:text-zinc-400")
                               :oic-outer ,(lts "text-zinc-500"
                                                "border"
                                                "border-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500")))))

                    :oic-outer,(lts "flex"
                                    "items-center"
                                    "w-full"
                                    "leading-normal")

                    :oic-radio ,(lts "flex"
                                     "items-center"
                                     "p-2"
                                     "leading-normal"
                                     "h-full")
                    :oic-radio-label ,(lts "flex-1"
                                           "w-full"
                                           "leading-none"
                                           "bg-transparent")

                    :class ,(lts "rounded" "focus-within:ring-2"))))

(setf (gethash "/radio" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (let ((cl-naive-tailwind-kit:*theme* nil))
               (cl-who:htm
                (:div :class "p-3"
                      (:h2 :class  "uppercase" "Radio examples build with raw classes:")
                      (:hr)
                      (:br)
                      (:tw-radio "Vanilla Text Radio.")
                      (:br)
                      (:div :class "grid grid-flow-row grid-cols-2 gap-2"

                            (:tw-radio :name "radio-left" :class "radio-left"
                                       "Text radio with radio to left.")
                            (:tw-radio :name "radio-right" :class "radio-right"
                                       "Text radio with radio to right.")
                            (:tw-radio :name "radio-left" :class "radio-left text-sm"
                                       "text-sm text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-sm"
                                       "text-sm text with radio right.")
                            (:tw-radio :name "radio-left" :class "radio-left text-lg"
                                       "text-lg text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-lg"
                                       "text-lg text with radio right.")
                            (:tw-radio :name "radio-left" :class "radio-left text-xl"
                                       "text-xl text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-xl"
                                       "text-xl text with radio right.")
                            (:tw-radio :name "radio-left" :class "radio-left text-2xl"
                                       "text-2xl text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-2xl"
                                       "text-2xl text with radio right.")
                            (:tw-radio :name "radio-left" :class "radio-left text-3xl"
                                       "text-3xl text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-3xl"
                                       "text-3xl text with radio right. ")
                            (:tw-radio :name "radio-left" :class "radio-left text-4xl"
                                       "text-4xl text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-4xl"
                                       "text-4xl text with radio right. ")
                            (:tw-radio :name "radio-left" :class "radio-left text-5xl"
                                       "text-5xl text with radio left.")
                            (:tw-radio :name "radio-right" :class "radio-right text-5xl"
                                       "text-5xl text with radio right. ")))))
             (:hr)
             (:br)
             (:div :class "p-3 dark:bg-zinc-900"
                   (:h2 :class "uppercase dark:text-zinc-400"
                        "Radio examples based on theme:")
                   (:hr)
                   (:br)
                   (:tw-radio :class "primary"
                              :name "primary-left"
                              "Vanilla Text Radio.")
                   (:br)
                   (:tw-radio :class "primary outline"
                              :name "primary-left"
                              "Vanilla Text Radio.")
                   (:br)
                   (:tw-radio :class "primary radio-left text-xl"
                              :name "primary-left"
                              "text-xl text with radio left.")
                   (:br)
                   (:tw-radio :class "primary outline radio-right text-xl"
                              :name "primary-right"
                              "text-xl text with radio right. ")
                   (:br)
                   (:tw-radio :class "secondary"
                              :name "secondary-left"
                              "Vanilla Text Radio.")
                   (:br)
                   (:tw-radio :class "secondary outline"
                              :name "secondary-left"
                              "Vanilla Text Radio.")
                   (:br)
                   (:tw-radio :class "secondary radio-left text-xl"
                              :name "secondary-left"
                              "text-xl text with radio left.")
                   (:br)
                   (:tw-radio :class "secondary outline radio-right text-xl"
                              :name "secondary-right"
                              "text-xl text with radio right. "))

             (:br)))))
