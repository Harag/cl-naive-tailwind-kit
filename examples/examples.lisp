(in-package :tailwind-kit.examples.clean)

(defparameter *server* (make-instance 'cl-naive-webserver.hunchentoot::hunchentoot-server
                                      :id "simple"
                                      :port 4444))

(register-server *server*)

(defmacro project-pathname ()
  (let ((path (asdf:system-source-directory :cl-naive-tailwind-kit.examples)))
    `(progn ,path)))

(start-server *server*)

(defparameter *site* (register-site
                      (make-instance 'site  :url "/simple")))

(setf (gethash "/tailwind.css" (handlers *site*))
      (format nil "~A~A"
              (project-pathname)
              "src/web/tailwind.css"))

(setf (gethash "/tailwind.css" (handlers *site*))
      (format nil "~A~A"
              (project-pathname)
              "examples/web/tailwind.css"))

(setf (gethash "/minion.svg" (handlers *site*))
      (format nil "~A~A"
              (project-pathname)
              "examples/web/minion.svg"))

(setf (gethash "/minion-white.svg" (handlers *site*))
      (format nil "~A~A"
              (project-pathname)
              "examples/web/minion-white.svg"))

(deftag (:page)
  ;;TODO: doctype is not being output, find a solution....
  "<!doctype html>"
  (:html :class "dark"
         (:head
          (:meta :charset "UTF-8")
          (:meta :name "viewport" :content "width=device-width, initial-scale=1.0")
          (:link :rel "icon" :type "text/css" :href "minion-white.svg")
          (:link :rel "stylesheet" :type "text/css" :href "tailwind.css")

          (:style ".minion {
  -webkit-mask: url(minion.svg) no-repeat center;
  mask-image: url(minion.svg) no-repeat center;
}")
          (:script :src "https://use.fontawesome.com/4ca16da5d8.js" :crossorigin "anonymous")

          (:script :src "https://kit.fontawesome.com/3c06307cb9.js" :crossorigin "anonymous")

          (:script
           "function toggleMode () {
    if(document.documentElement.classList.contains('dark')){
        document.documentElement.classList.remove('dark');
    }
    else {
        document.documentElement.classList.add('dark');
    }

}"))

         (:body :class "p-2"
                (:button :onclick "toggleMode();"
                         :class "border border-zinc-900 dark:bg-zinc-700 dark:text-zinc-400 rounded p-2"
                         "Toggle Between Dark and Light")
                (:br)
                (:br)
                (new-children))))

