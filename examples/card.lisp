(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-card
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-outer ,(lts "text-blue-500"

                                                "focus-within:ring-blue-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))
                              (:name :outline
                               :oic-outer ,(lts "text-blue-500"

                                                "border"
                                                "border-blue-400"
                                                "focus-within:ring-blue-400"
                                                "dark:border-zinc-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))))
                     (:name "secondary"
                      :types ((:name :default
                               :oic-outer ,(lts
                                            "text-zinc-500"

                                            "focus-within:ring-zinc-500"
                                            "dark:bg-zinc-800"
                                            "dark:text-zinc-400"
                                            "dark:focus-within:ring-zinc-500"))
                              (:name :outline
                               :oic-outer ,(lts
                                            "text-zinc-500"
                                            "border"
                                            "border-zinc-500"
                                            "focus-within:ring-zinc-500"
                                            "dark:bg-zinc-800"
                                            "dark:text-zinc-400"
                                            "dark:focus-within:ring-zinc-500")))))

                    :oic-outer ,(lts "w-full"
                                     "overflow-hidden")

                    :class ,(lts "rounded"))))

(setf (gethash "/card" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (:div :class "flex"
                   (let ((cl-naive-tailwind-kit:*theme* nil))
                     (cl-who:htm
                      (:div :class "p-3 max-w-lg"
                            (:h2 :class  "uppercase" "Input examples build with raw classes:")

                            (:hr)
                            (:br)
                            (:tw-card :title "Vanilla Card"
                                      "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                            (:br)
                            (:tw-card :title "Vanilla Card"
                                      :src "minion.svg"
                                      "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                            (:br)
                            (:tw-card :class "flex"
                                      :image-class "h-24 w-24 p-2"
                                      :title "Vanilla Card"
                                      :src "minion.svg"

                                      "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                            (:br))))

                   (:div :class "p-3 dark:bg-zinc-900 max-w-lg"
                         (:h2 :class "uppercase dark:text-zinc-400"
                              "TextArea examples based on theme:")
                         (:hr)
                         (:br)
                         (:tw-card :class "primary"
                                   :title "Themed Card"
                                   "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                         (:br)
                         (:tw-card :class "primary outline text-xl"
                                   :title "Themed Card"
                                   "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                         (:br)
                         (:tw-card :class "secondary"
                                   :title "Themed Card"
                                   :src "minion.svg"
                                   "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                         (:br)
                         (:tw-card :class "flex secondary outline text-xl"
                                   :image-class "h-24 w-24 p-2"
                                   :title "Themed Card"
                                   :src "minion.svg"
                                   "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.")
                         (:br)))))))
