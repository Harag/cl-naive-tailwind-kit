(in-package :tailwind-kit.examples.clean)

(setf (gethash "/test" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (let ((cl-naive-tailwind-kit:*theme* nil))
               (cl-who:htm
                (:div :class "p-3"
                      (:h2 :class  "uppercase" "Select examples build with raw classes:")

                      (:hr)
                      (:br)
                      (:tw-input :value "Vanilla Text Input.")
                      (:div :class "grid "

                            (:tw-select)))))
             (:hr)
             (:tw-test)
             (:br)
             (:div :class "p-3 dark:bg-zinc-900"
                   (:h2 :class "uppercase dark:text-zinc-400"
                        "Input examples based on theme:")
                   (:hr)
                   (:br)

                   (:br))))))

