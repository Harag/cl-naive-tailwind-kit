(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-textarea
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-textarea ,(lts "text-blue-500"

                                                   "focus-within:ring-blue-400"
                                                   "dark:bg-zinc-700"
                                                   "dark:text-zinc-400"
                                                   "dark:focus-within:ring-zinc-400"))
                              (:name :outline
                               :oic-textarea ,(lts "text-blue-500"

                                                   "border"
                                                   "border-blue-400"
                                                   "focus-within:ring-blue-400"
                                                   "dark:border-zinc-400"
                                                   "dark:bg-zinc-700"
                                                   "dark:text-zinc-400"
                                                   "dark:focus-within:ring-zinc-400"))))
                     (:name "secondary"
                      :types ((:name :default
                               :oic-textarea ,(lts
                                               "text-zinc-500"

                                               "focus-within:ring-zinc-500"
                                               "dark:bg-zinc-800"
                                               "dark:text-zinc-400"
                                               "dark:focus-within:ring-zinc-500"))
                              (:name :outline
                               :oic-textarea ,(lts
                                               "text-zinc-500"
                                               "border"
                                               "border-zinc-500"
                                               "focus-within:ring-zinc-500"
                                               "dark:bg-zinc-800"
                                               "dark:text-zinc-400"
                                               "dark:focus-within:ring-zinc-500")))))

                    :oic-textarea ,(lts "w-full"
                                        "px-2"
                                        "py-2")

                    :class ,(lts "rounded" "focus-within:ring-2"))))

(setf (gethash "/textarea" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (let ((cl-naive-tailwind-kit:*theme* nil))
               (cl-who:htm
                (:div :class "p-3"
                      (:h2 :class  "uppercase" "Input examples build with raw classes:")

                      (:hr)
                      (:br)
                      (:tw-textarea :class "focus:outline-none" "Vanilla TextArea.")
                      (:br)
                      (:tw-textarea :class "rounded focus:outline-none text-xl"
                                    "Vanilla TextArea."))))

             (:div :class "p-3 dark:bg-zinc-900"
                   (:h2 :class "uppercase dark:text-zinc-400"
                        "TextArea examples based on theme:")
                   (:hr)
                   (:br)
                   (:tw-textarea :class "primary" "Themed TextArea")
                   (:br)
                   (:tw-textarea :class "primary outline text-xl" "Themed TextArea.")
                   (:br)
                   (:tw-textarea :class "secondary" "Themed TextArea")
                   (:br)
                   (:tw-textarea :class "secondary outline text-xl" "Themed TextArea.")
                   (:br))))))
