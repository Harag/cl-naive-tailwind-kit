(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-select
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-select ,(lts "text-blue-500"
                                                 "dark:text-zinc-400")

                               :oic-outer ,(lts "text-blue-500"
                                                "focus-within:ring-blue-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))
                              (:name :outline
                               :oic-select ,(lts "text-blue-500"
                                                 "dark:text-zinc-400")
                               :oic-outer ,(lts "text-blue-500"
                                                "border"
                                                "border-blue-400"
                                                "focus-within:ring-blue-400"

                                                "dark:border-zinc-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))))
                     (:name "secondary"
                      :types ((:name :default
                               :oic-select ,(lts
                                             "text-zinc-500"

                                             "dark:text-zinc-400")

                               :oic-outer ,(lts "text-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500"))
                              (:name :outline
                               :oic-select ,(lts
                                             "text-zinc-500"
                                             "dark:text-zinc-400")
                               :oic-outer ,(lts "text-zinc-500"
                                                "border"
                                                "border-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500")))))

                    :oic-outer ,(lts "flex"
                                     "items-center"
                                     "w-full"
                                     "leading-normal"
                                     "content-center")

                    :oic-select ,(lts "flex-1"
                                      "w-full"
                                      "leading-normal"
                                      "bg-transparent"
                                      "focus:outline-none")

                    :oic-minion ,(lts "flex"
                                      "items-center"
                                      "p-2"
                                      "leading-normal"
                                      "h-full")

                    :minion (:i :class "fas fa-poo")

                    :minion-class nil

                    :select-class nil

                    :class ,(lts "rounded" "focus-within:ring-2")))

  (set-theme-item `(:tag :tw-option
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-option ,(lts "text-blue-500"
                                                 "dark:text-zinc-400"
                                                 "dark:bg-zinc-700")))
                      (:name "secondary"
                       :types ((:name :default
                                :oic-select ,(lts
                                              "text-zinc-500"
                                              "dark:text-zinc-400"
                                              "dark:bg-zinc-800"))))))

                    :class nil)))

(defun options (options &optional class)
  (with-html-to-string
      (cl-who:htm
       (dolist (option options)
         (cl-who:htm (:tw-option :class class (cl-who:str option)))))))

(setf (gethash "/select" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (let ((cl-naive-tailwind-kit:*theme* nil)
                   (options '("Option A"
                              "Option B"
                              "Option C")))

               (cl-who:htm
                (:div :class "p-3"
                      (:h2 :class  "uppercase" "Select examples build with raw classes:")

                      (:hr)
                      (:br)
                      (:tw-select :value "Vanilla Text Select."
                                  (:option :value ""
                                           :readonly t
                                           :selected t
                                           :hidden t
                                           "Select an option")
                                  (:option "Option A")
                                  (:option "Option B")
                                  (:option "Option C"))
                      (:br)

                      (:div :class "grid grid-flow-row grid-cols-2 gap-2"

                            (:tw-select :class "minion-left"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))

                            (:tw-select :class "minion-left text-sm"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))

                            (:tw-select :class "minion-right text-base"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-left text-lg"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right text-lg"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-left text-xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right text-xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-left text-2xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right text-2xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-left text-3xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right text-3xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-left text-4xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right text-4xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-left text-5xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))
                            (:tw-select :class "minion-right text-5xl"
                                        :minion (:div :class "bg-black minion h-em w-em")
                                        (cl-who:str (options options)))))))
             (:hr)
             (:br)
             (let ((options '("Option A"
                              "Option B"
                              "Option C")))
               (cl-who:htm
                (:div :class "p-3 dark:bg-zinc-900"
                      (:h2 :class "uppercase dark:text-zinc-400"
                           "Select examples based on theme:")
                      (:hr)
                      (:br)
                      (:tw-select :class "primary"
                                  (cl-who:str (options options "primary")))
                      (:br)
                      (:tw-select :class "primary outline"
                                  (cl-who:str (options options "primary")))
                      (:br)
                      (:tw-select :class "primary minion-left text-xl"
                                  :minion (:div :class "bg-black dark:bg-white minion h-em w-em")
                                  (cl-who:str (options options "primary")))
                      (:br)
                      (:tw-select :class "primary outline minion-right text-xl"
                                  :minion (:div :class "bg-black dark:bg-white minion h-em w-em")
                                  (cl-who:str (options options "primary")))
                      (:br)
                      (:tw-select :class "secondary"
                                  (cl-who:str (options options "secondary")))
                      (:br)
                      (:tw-select :class "secondary outline"
                                  (cl-who:str (options options "secondary")))
                      (:br)
                      (:tw-select :class "secondary minion-left text-xl"
                                  :minion (:div :class "bg-black dark:bg-white minion h-em w-em")
                                  (cl-who:str (options options "secondary")))
                      (:br)
                      (:tw-select :class "secondary outline minion-right text-xl"
                                  :minion (:div :class "bg-black dark:bg-white minion h-em w-em")
                                  (cl-who:str (options options "secondary"))))))

             (:br)))))

