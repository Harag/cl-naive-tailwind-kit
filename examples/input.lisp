(in-package :tailwind-kit.examples.clean)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (set-theme-item `(:tag :tw-input
                    :colors
                    ((:name "primary"
                      :types ((:name :default
                               :oic-input ,(lts "text-blue-500"
                                                "dark:text-zinc-400")

                               :oic-outer ,(lts "text-blue-500"
                                                "focus-within:ring-blue-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))
                              (:name :outline
                               :oic-input ,(lts "text-blue-500"
                                                "dark:text-zinc-400")
                               :oic-outer ,(lts "text-blue-500"
                                                "border"
                                                "border-blue-400"
                                                "focus-within:ring-blue-400"
                                                "dark:border-zinc-400"
                                                "dark:bg-zinc-700"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-400"))))
                     (:name "secondary"
                      :types ((:name :default
                               :oic-input ,(lts
                                            "text-zinc-500"

                                            "dark:text-zinc-400")

                               :oic-outer ,(lts "text-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500"))
                              (:name :outline
                               :oic-input ,(lts
                                            "text-zinc-500"
                                            "dark:text-zinc-400")
                               :oic-outer ,(lts "text-zinc-500"
                                                "border"
                                                "border-zinc-500"
                                                "focus-within:ring-zinc-500"
                                                "dark:bg-zinc-800"
                                                "dark:text-zinc-400"
                                                "dark:focus-within:ring-zinc-500")))))

                    :oic-outer ,(lts "flex"
                                     "items-center"
                                     "w-full"
                                     "leading-normal"
                                     "content-center")

                    :oic-input ,(lts "flex-1"
                                     "w-full"
                                     "px-2"
                                     "leading-normal"
                                     "bg-transparent"
                                     "focus:outline-none")

                    :oic-minion ,(lts "flex"
                                      "items-center"

                                      "leading-normal"
                                      "h-full")

                    :minion (:i :class "fas fa-poo")

                    :minion-class nil

                    :input-class nil

                    :class ,(lts "rounded" "focus-within:ring-2"))))

(setf (gethash "/input" (handlers *site*))
      #'(lambda (script-name)
          (declare (ignore script-name))

          (with-html-to-string
            "<!doctype html>"
            (::page
             (let ((cl-naive-tailwind-kit:*theme* nil))
               (cl-who:htm
                (:div :class "p-3"
                      (:h2 :class  "uppercase" "Input examples build with raw classes:")

                      (:hr)
                      (:br)
                      (:tw-input :value "Vanilla Text Input.")
                      (:br)

                      (:div :class "grid grid-flow-row grid-cols-2 gap-2"

                            (:tw-input :class "minion-left"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :value "Text input with minion to left.")
                            (:tw-input :class "minion-right"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :value "Text input with minion to right.")

                            (:tw-input :class "minion-left text-sm"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-sm text with minion left.")

                            (:tw-input :class "minion-right text-base"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-base text with minion right.")
                            (:tw-input :class "minion-left text-lg"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-lg text with minion left.")
                            (:tw-input :class "minion-right text-lg"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-lg text with minion right.")
                            (:tw-input :class "minion-left text-xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-xl text with minion left.")
                            (:tw-input :class "minion-right text-xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-xl text with minion right.")
                            (:tw-input :class "minion-left text-2xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-2xl text with minion left.")
                            (:tw-input :class "minion-right text-2xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-2xl text with minion right.")
                            (:tw-input :class "minion-left text-3xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-3xl text with minion left.")
                            (:tw-input :class "minion-right text-3xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-3xl text with minion right. ")
                            (:tw-input :class "minion-left text-4xl bg-red-200 rounded"
                                       :minion
                                       (:button :class "bg-blue-500 rounded-l h-full text-zinc-50"
                                                (:div :class "bg-white minion h-em w-em"))
                                       :placeholder "text-4xl text with minion button.")
                            (:tw-input :class "minion-right text-4xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-4xl text with minion right. ")
                            (:tw-input :class "minion-left text-5xl bg-red-200"
                                       :minion-class "bg-emerald-200"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-5xl text with minion left.")
                            (:tw-input :class "minion-right text-5xl"
                                       :minion (:div :class "bg-black minion h-em w-em")
                                       :placeholder "text-5xl text with minion right. ")))))
             (:hr)
             (:br)
             (:div :class "p-3 dark:bg-zinc-900"
                   (:h2 :class "uppercase dark:text-zinc-400"
                        "Input examples based on theme:")
                   (:hr)
                   (:br)
                   (:tw-input :class "primary" :value "Vanilla Text Input.")
                   (:br)
                   (:tw-input :class "primary outline" :value "Vanilla Text Input.")
                   (:br)
                   (:tw-input :class "primary minion-left text-xl"
                              :minion-class "bg-emerald-200"
                              :minion (:div :class "m-1.5 bg-black minion h-em w-em")
                              :placeholder "text-xl text with minion left.")
                   (:br)
                   (:tw-input :class "primary outline minion-right text-xl"
                              :minion (:button :class "h-full bg-blue-500 rounded-r focus:outline-none"
                                               (:div :class "m-1.5 bg-white minion h-em w-em"))
                              :placeholder "text-xl text with minion right button. ")
                   (:br)
                   (:tw-input :class "secondary" :value "Vanilla Text Input.")
                   (:br)
                   (:tw-input :class "secondary outline" :value "Vanilla Text Input.")
                   (:br)
                   (:tw-input :class "secondary minion-left text-xl"
                              :minion (:div :class "bg-zinc-900 dark:bg-white minion h-em w-em")
                              :placeholder "xl-base text with minion left.")
                   (:br)
                   (:tw-input :class "secondary outline minion-right text-xl"
                              :minion (:div :class "bg-zinc-900 dark:bg-white minion h-em w-em")
                              :placeholder "text-xl text with minion right. "))

             (:br)))))

