(in-package :cl-naive-tailwind-kit.tests)

(define-config (:tw.tw-alert)
  (:tw.config
   (:tw.alert-box
    :class '("px-6"
             "py-4"
             "border-0"
             "rounded"
             "relative"
             "mb-4"))
   (:tw.alert-icon
    :class '("text-xl" "inline-block" "mr-5" "align-middle" "theme-marker")
    :icon-class nil)
   (:tw.alert-body
    :class '("inline-block" "align-middle" "mr-8"))
   (:tw.alert-button
    :class '("absolute"
             "bg-transparent"
             "text-normal"
             "font-semibold"
             "leading-none"
             "right-0"
             "top-0"
             "mt-5"
             "mr-4"
             "outline-none"
             "focus:outline-none")
    :icon-class '("fa" "fa-times")
    :onclick "this.parentNode.style.display=\"none\";")))

(testsuite  :tw-alert
  (testcase :alert-simple-no-theme
            :expected '(:DIV :CLASS ("z-0") (:SPAN :CLASS ("z-20") "Alert")
                        (:BUTTON :ONCLICK "this.parentNode.style.display='none';" (:I)))
            :actual (with-current-package
                      (let ((cl-naive-ccf::*configs* (make-hash-table)))
                        (with-sexp
                          (expand
                           (with-dom
                             (:tw.tw-alert "Alert"))))))
            :info "Test vanilla alert with no theme.")

  (testcase :alert-simple-theme
            :expected ' (:DIV :CLASS ("z-0" "px-6" "py-4" "border-0" "rounded" "relative" "mb-4")
                              (:SPAN :CLASS ("text-xl" "inline-block" "mr-5" "align-middle" "theme-marker")
                                     (:I :CLASS ("fa" "fa-times")))
                              (:SPAN :CLASS ("z-20" "inline-block" "align-middle" "mr-8") "Alert")
                              (:BUTTON :CLASS
                                       ("absolute" "bg-transparent" "text-normal" "font-semibold" "leading-none"
                                                   "right-0" "top-0" "mt-5" "mr-4" "outline-none" "focus:outline-none")
                                       :ONCLICK "this.parentNode.style.display=\"none\";"
                                       (:I :CLASS ("fa" "fa-times"))))
            :actual (with-current-package
                      (with-sexp
                        (expand
                         (with-dom
                           (:tw.tw-alert :icon '("fa" "fa-times")
                                         "Alert")))))
            :info "Test vanilla alert with theme.")

  #|

  (testcase :alert-class
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 class-marker'>
  <span class='inline-block align-middle mr-8'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (print (let ((*theme*))
  (with-dom
  (:tw-alert :class "class-marker"
  "Alert"))))
  :info "class passed as attribute")

  (testcase :alert-oic-outer-div-no-theme
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 mark-override'>
  <span class='inline-block align-middle mr-8'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (let ((*theme*))
  (with-dom
  (:tw-alert
  :oic-outer-div (lts
  "px-6"
  "py-4"
  "border-0"
  "rounded"
  "relative"
  "mb-4"
  "mark-override")
  "Alert")))
  :info "Test vanilla alert with no theme.")

  (testcase :alert-oic-outer-div-no-theme
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 mark-override'>
  <span class='inline-block align-middle mr-8'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (let ((*theme*))
  (with-dom
  (:tw-alert
  :oic-outer-div (lts
  "px-6"
  "py-4"
  "border-0"
  "rounded"
  "relative"
  "mb-4"
  "mark-override")
  "Alert")))
  :info "oic-outer-div set in attributes.")

  (testcase :alert-oic-outer-div-theme-1
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 theme-marker rounded'>
  <span class='inline-block align-middle mr-8 theme-marker'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none theme-marker' onclick='this.parentNode.style.display=\"none\"; alert(\"Close\");'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (with-dom
  (:tw-alert
  "Alert"))
  :info "oic-outer-div set in theme.")

  (testcase :alert-oic-outer-div-theme-2
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 mark-override rounded'>
  <span class='inline-block align-middle mr-8 theme-marker'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none theme-marker' onclick='this.parentNode.style.display=\"none\"; alert(\"Close\");'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (with-dom
  (:tw-alert
  :oic-outer-div (lts
  "px-6"
  "py-4"
  "border-0"
  "rounded"
  "relative"
  "mb-4"
  "mark-override")
  "Alert"))
  :info "oic-outer-div passed in attributes overrides theme.")

  (testcase :alert-class-theme-color-class
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 theme-marker bg-blue-500 text-blue-50 dark:bg-zinc-700 dark:text-zinc-400 rounded primary'>
  <span class='inline-block align-middle mr-8 theme-marker'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none theme-marker' onclick='this.parentNode.style.display=\"none\"; alert(\"Close\");'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (with-dom
  (:tw-alert :class "primary"
  "Alert"))
  :info "color class passed as attribute for theme")

  (testcase :alert-oic-body-no-theme
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4'>
  <span class='inline-block align-middle mr-8 mark-override'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (let ((*theme*))
  (with-dom
  (:tw-alert
  :oic-body (lts
  "inline-block"
  "align-middle"
  "mr-8"
  "mark-override")
  "Alert")))
  :info "oic-body set in attributes.")

  (testcase :alert-oic-body-theme-override
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 theme-marker rounded'>
  <span class='inline-block align-middle mr-8 mark-override'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none theme-marker' onclick='this.parentNode.style.display=\"none\"; alert(\"Close\");'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual(with-dom
  (:tw-alert
  :oic-body (lts
  "inline-block"
  "align-middle"
  "mr-8"
  "mark-override")
  "Alert"))
  :info "oic-body set in attributes overides-theme.")

  (testcase :alert-oic-body-no-theme
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4'>
  <span class='inline-block align-middle mr-8'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none mark-override' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (let ((*theme*))
  (with-dom
  (:tw-alert
  :oic-close-button (lts
  "absolute"
  "bg-transparent"
  "text-normal"
  "font-semibold"
  "leading-none"
  "right-0"
  "top-0"
  "mt-5"
  "mr-4"
  "outline-none"
  "focus:outline-none"
  "mark-override")
  "Alert")))
  :info "oic-close-button set in attributes.")

  (testcase :alert-oic-close-button-theme-override
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 theme-marker rounded'>
  <span class='inline-block align-middle mr-8 theme-marker'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none mark-override' onclick='this.parentNode.style.display=\"none\"; alert(\"Close\");'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (with-dom
  (:tw-alert
  :oic-close-button (lts
  "absolute"
  "bg-transparent"
  "text-normal"
  "font-semibold"
  "leading-none"
  "right-0"
  "top-0"
  "mt-5"
  "mr-4"
  "outline-none"
  "focus:outline-none"
  "mark-override")
  "Alert"))
  :info "oic-close-button set in attributes overides-theme.")

  (testcase :alert-oic-icon-no-theme
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4'>
  <span class='text-xl inline-block mr-5 align-middle mark-override'>
  <i class='fa fa-poop'></i>
  </span>
  <span class='inline-block align-middle mr-8'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (let ((*theme*))
  (with-dom
  (:tw-alert
  :icon "fa fa-poop"
  :oic-icon (lts "text-xl"
  "inline-block"
  "mr-5"
  "align-middle"
  "mark-override")
  "Alert")))
  :info "oic-icon set in attributes.")
  (testcase :alert-oic-icon-theme-override
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4 theme-marker rounded'>
  <span class='text-xl inline-block mr-5 align-middle mark-override'>
  <i class='fa fa-poop'></i>
  </span>
  <span class='inline-block align-middle mr-8 theme-marker'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none theme-marker' onclick='this.parentNode.style.display=\"none\"; alert(\"Close\");'>
  <i class='fa fa-times'></i>
  </button>
  </div>"
  :actual (with-dom
  (:tw-alert
  :icon "fa fa-poop"
  :oic-icon (lts "text-xl"
  "inline-block"
  "mr-5"
  "align-middle"
  "mark-override")
  "Alert"))
  :info "oic-icon set in attributes overides-theme.")
  (testcase :alert-close-icon
  :expected "
  <div class='px-6 py-4 border-0 rounded relative mb-4'>
  <span class='inline-block align-middle mr-8'>Alert
  </span>
  <button class='absolute bg-transparent text-normal font-semibold leading-none right-0 top-0 mt-5 mr-4 outline-none focus:outline-none' onclick='this.parentNode.style.display=\"none\";'>
  <i class='fa fa-poop'></i>
  </button>
  </div>"
  :actual (let ((*theme*))
  (with-dom
  (:tw-alert
  :close-icon "fa fa-poop"
  "Alert")))
  :info "Test close icon")

  |#)

;;(report (run))
