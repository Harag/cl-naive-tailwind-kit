(in-package :cl-naive-tailwind-kit.tests)

(define-config (:tw.tw-button)
  (:tw.config
   (:tw.color-scheme
    (:tw.primary
     (:tw.default
      ())
     (:tw.outline
      ()))
    (:tw.secondary
     (:tw.default
      ())
     (:tw.outline
      ())))
   (:tw.round-icon
    :class '("inline-flex"
             "items-center"
             "justify-center"
             "mr-2"
             "rounded-full"
             "theme-marker")
    :icon-class '("fas fa-poo"))
   (:tw.pill
    :class '("font-bold"
             "px-4"
             "py-2"
             "mr-1"
             "mb-1"
             "rounded-full"
             "theme-marker"))
   (:tw.default
    :class '("font-bold"
             "px-4"
             "py-2"
             "mr-1"
             "mb-1"
             "theme-marker"))))

(testsuite  :tw-button

  (testcase :button-simple-no-theme
            :expected '(:BUTTON "Button")
            :actual (with-current-package
                      (let ((cl-naive-ccf::*configs* (make-hash-table)))
                        (with-sexp
                          (expand
                           (with-dom
                             (:tw.tw-button "Button"))))))
            :info "Test vanilla button with no theme.")

  (testcase :button-simple-theme
            :expected '(:BUTTON :CLASS
                        ("primary" "font-bold" "px-4" "py-2" "mr-1" "mb-1" "theme-marker") "Button")
            :actual (with-current-package
                      (with-sexp
                        (expand
                         (with-dom
                           (:tw.tw-button :class '("primary") "Button")))))
            :info "Test vanilla button with theme.")
  #|
  (testcase :button-class-no-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 pink'>Button
  </button>"
  :actual (let ((*theme*))
  (with-html-to-string
  (:tw-button
  :class "pink"
  "Button")))
  :info "Button class with no theme.")

  (testcase :button-class-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 theme-marker outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded pink'>Button
  </button>"
  :actual (print (with-html-to-string
  (:tw-button
  :class "pink"
  "Button")))
  :info "Button class with theme.")

  (testcase :button-oic-button-no-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 mark-override'>Button
  </button>"
  :actual (let ((*theme*))
  (with-html-to-string
  (:tw-button
  :oic-button (lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
  "mark-override")
  "Button")))
  :info "oic-button with no theme.")
  (testcase :button-oic-button-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 mark-override outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded'>Button
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :oic-button (lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
  "mark-override")
  "Button"))
  :info "oic-button in attributes overrides theme."))

  (sb-ext:gc :full t)

  (testsuite  :tw-button-round-icon

  (testcase :button-round-icon-simple-no-theme
  :expected "
  <button class='inline-flex items-center justify-center mr-2 rounded-full w-10 h-10 round-icon'>
  <i class='fa fa-poop'></i>
  </button>"
  :actual (let ((*theme*))
  (with-html-to-string
  (:tw-button
  :class "round-icon"
  :icon "fa fa-poop"
  "Button")))
  :info "Test vanilla button with no theme.")

  (testcase :button--round-iconsimple-theme
  :expected "
  <button class='inline-flex items-center justify-center mr-2 rounded-full w-10 h-10 outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded round-icon'>
  <i class='fa fa-poop'></i>
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "round-icon"
  :icon "fa fa-poop"
  "Button"))
  :info "Test vanilla button with theme.")

  (testcase :button-round-icon-class-no-theme
  :expected "
  <button class='inline-flex items-center justify-center mr-2 rounded-full w-10 h-10 round-icon pink'>
  <i class='fa fa-poop'></i>
  </button>"
  :actual (let ((*theme*))
  (with-html-to-string
  (:tw-button
  :class "round-icon pink"
  :icon "fa fa-poop"
  "Button")))
  :info "Button class with no theme.")

  (testcase :button-round-icon-class-theme
  :expected "
  <button class='inline-flex items-center justify-center mr-2 rounded-full w-10 h-10 outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded round-icon pink'>
  <i class='fa fa-poop'></i>
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "round-icon pink"
  :icon "fa fa-poop"
  "Button"))
  :info "Button class with theme.")

  (testcase :button-round-icon-oic-button-no-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 mark-override w-10 h-10 outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded round-icon'>
  <i class='fa fa-poop'></i>
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "round-icon"
  :icon "fa fa-poop"
  :oic-button (lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
  "mark-override")
  "Button"))
  :info "oic-button with no theme.")
  (testcase :button-round-icon-oic-button-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 mark-override w-10 h-10 outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded round-icon'>
  <i class='fa fa-poop'></i>
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "round-icon"
  :icon "fa fa-poop"
  :oic-button (lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
  "mark-override")
  "Button"))
  :info "oic-button in attributes overrides theme."))

  (testsuite  :tw-button-pill

  (testcase :button-pill-simple-no-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 rounded-full pill' icon='fa fa-poop'>Button
  </button>"
  :actual (let ((*theme*))
  (with-html-to-string
  (:tw-button
  :class "pill"
  :icon "fa fa-poop"
  "Button")))
  :info "Test vanilla button with no theme.")

  (testcase :button--pillsimple-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 rounded-full theme-marker outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded pill' icon='fa fa-poop'>Button
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "pill"
  :icon "fa fa-poop"
  "Button"))
  :info "Test vanilla button with theme.")

  (testcase :button-pill-class-no-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 rounded-full pill pink' icon='fa fa-poop'>Button
  </button>"
  :actual (let ((*theme*))
  (with-html-to-string
  (:tw-button
  :class "pill pink"
  :icon "fa fa-poop"
  "Button")))
  :info "Button class with no theme.")

  (testcase :button-pill-class-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 rounded-full theme-marker outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded pill pink' icon='fa fa-poop'>Button
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "pill pink"
  :icon "fa fa-poop"
  "Button"))
  :info "Button class with theme.")

  (testcase :button-pill-oic-button-no-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 mark-override outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded pill' icon='fa fa-poop'>Button
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "pill"
  :icon "fa fa-poop"
  :oic-button (lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
  "mark-override")
  "Button"))
  :info "oic-button with no theme.")
  (testcase :button-pill-oic-button-theme
  :expected "
  <button class='font-bold px-4 py-2 mr-1 mb-1 mark-override outline-none focus:outline-none ease-linear transition-all duration-150 shadow-none rounded pill' icon='fa fa-poop'>Button
  </button>"
  :actual (with-html-to-string
  (:tw-button
  :class "pill"
  :icon "fa fa-poop"
  :oic-button (lts "font-bold" "px-4" "py-2" "mr-1" "mb-1"
  "mark-override")
  "Button"))
  :info "oic-button in attributes overrides theme.")
  |#)

;;(report (run))
