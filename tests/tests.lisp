(in-package :cl-naive-tailwind-kit.tests)

(defmacro with-current-package (&body body)
  `(let ((*package* (or (find-package #.(package-name *package*))
                        (error "Cannot find the package named ~S"
                               #.(package-name *package*)))))
     ,@body))

;;(cl-naive-tests:report (cl-naive-tests:run))
