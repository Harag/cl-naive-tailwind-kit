const colors = require('tailwindcss/colors')

module.exports = {
    content: [
        // Example content paths...
        './src/**/*.{js,lisp}',
    ],

    darkMode: 'class', // false or 'media' or 'class'
    theme: {
        extend: {

        }
    },
    plugins: [

    ],
}

