(in-package :common-lisp-user)

(defpackage :cl-naive-tailwind-kit
  (:use :cl :cl-naive-dom :cl-naive-dom.abt :cl-naive-dom.js :cl-naive-ccf)
  (:local-nicknames (:tw :cl-naive-tailwind-kit))
  (:export
   :*theme*
   :set-theme-item
   :remove-theme-item
   :tag-theme
   :tag-theme-color))

