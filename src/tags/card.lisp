(in-package :cl-naive-tailwind-kit)

(define-config (:tw-card)
  (:config
   (:card-box
    :class '("w-full"
             "overflow-hidden"))
   (:card-image
    :class '("object-cover"))
   (:card-container
    '("flex-1"))
   (:card-title
    :class '("font-semibold"
             "p-2")
    :size 4)
   (:card-content
    :class '("px-2"))))

(deftag (:card-box)
  (:div :class (merge-classes
                '("z-0")
                (att :class)
                (tag-setting :class :tw.tw-card))
        (new-children)))

(deftag (:card-image)
  (:div ;;:class '("justify-center")

   (:image :class (merge-classes
                   (att :class)
                   (tag-setting :class :tw.tw-card)
                   '("dark:hidden"))

           :src (att :src)
           :alt (att :alt))

   (:image :class (merge-classes
                   (att :class)
                   (tag-setting :class :tw.tw-card)
                   '("hidden"
                     "dark:block"))
           :src (or (att :dark-src) (att :src))
           :alt (att :alt))))

(deftag (:card-container)
  (:div :class (merge-classes
                '("relative"
                  "bg-transparent"
                  "z-20")
                (att :class)
                (tag-setting :class :tw.tw-card))

        (new-children)))

(deftag (:card-title)
  (:h :size (or (att :size)
                (tag-setting :size :tw.tw-card))
      :class (merge-classes
              (att :class)
              (tag-setting :class :tw.tw-card))
      (new-children)))

(deftag (:card-content)
  (:div :class (merge-classes
                (att :class)
                (tag-setting :class :tw.tw-card)
                '("relative"
                  "bg-transparent"
                  "z-30"))
        (new-children)

        (when (att :watermark)
          (:watermark :class (att :watermark)))))

(deftag (:watermark)
  (:div :class '("absolute"
                 "top-0"
                 "left-0"
                 "z-10"
                 "text-5xl")
        (:i :class (merge-classes '("z-10") (att :class)))))

(deftag (:tw-card)
  (:card-box :class (att :class)
             (when (att :image-src)
               (:card-image
                :class (att :image-class)
                :src (att :image-src)
                :dark-src (att :image-dark-src)
                :alt (att :image-alt)))
             (:card-container :class (att :container-class)
                              (:card-title :size (att :title-size)
                                           :class (att :title-class)
                                           (att :title))
                              (:card-content :class (att :content-class)
                                             :watermark (att :content-watermark)
                                             (new-children))
                              (when (att :watermark)
                                (:watermark :class (att :watermark))))))

(with-sexp
  (expand
   (with-dom
     (:tw-card :class '("rounded")
               "Oh Fuck!"))))
