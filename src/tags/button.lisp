(in-package :cl-naive-tailwind-kit)

(define-config (:tw-button)
  (:config
   (:round-icon
    :class '("inline-flex"
             "items-center"
             "justify-center"
             "mr-2"
             "rounded-full")
    :icon-class '("fas fa-poo"))
   (:pill
    :class '("font-bold"
             "px-4"
             "py-2"
             "mr-1"
             "mb-1"
             "rounded-full"))
   (:default
    :class '("font-bold"
             "px-4"
             "py-2"
             "mr-1"
             "mb-1"))))

(deftag (:tw-button)
  (:button :class (merge-classes
                   (att :class)

                   (cond ((class-exist "round-icon" (att :class))
                          (merge-classes
                           (if (att :size)
                               (list (format nil "h-~A" (att :size))
                                     (format nil "w-~A" (att :size)))
                               (list "w-10" "h-10"))
                           (setting :tw.round-icon :class :tw.tw-button)))
                         ((class-exist "pill" (att :class))
                          (setting :tw.pill :class :tw.tw-button))
                         (t
                          ;;Have to be specific there are multiple
                          ;;defauts in the config.
                          (setting '(:config :tw.default) :class :tw.tw-button))))

           (if (class-exist "round-icon" (att :class))
               (:i :class (or (att :icon)
                              (setting :tw.round-icon :icon-class :tw.tw-button)))
               (new-children))))

#|
(with-sexp
(expand
(with-dom
(:tw-button :class '("primary" "pill")
"Oh Fuck!"))))

(with-sexp
(expand
(with-dom
(:tw-button :class '("primary" "round-icon")
"Oh Fuck!"))))

(with-sexp
(expand
(with-dom
(:tw-button :class '("primary")
"Oh Fuck!"))))
|#
