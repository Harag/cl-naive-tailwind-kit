(in-package :cl-naive-tailwind-kit)

(define-config (:tw-checkbox)
  (:config
   (:checkbox-box
    :class '("flex"
             "items-center"
             "w-full"
             "leading-normal"
             "focus-within:ring-2"))
   (:checkbox-check
    :class '("flex"
             "items-center"
             "p-2"
             "leading-normal"
             "h-full")
    :input-class '("align-middle"
                   "leading-normal"
                   "h-em"
                   "w-em"))
   (:checkbox-label
    :class '("flex-1"
             "w-full"
             "leading-none"
             "bg-transparent"))))

(deftag (:checkbox-box)
  (:div :class (merge-classes
                (typecase (att :class)
                  (string (list (concatenate 'string (att :class) " ")))
                  (list (mapcar (lambda (class)
                                  (replace-all class "focus:" "focus-within:"))
                                (att :class))))
                (tag-setting :class :tw-checkbox))

        (new-children)))

(deftag (:checkbox-check)
  (:span :class (merge-classes
                 (att :class)
                 (tag-setting :class :tw-checkbox))

         (:input :type "checkbox"
                 :class (merge-classes
                         (att :input-class)
                         (tag-setting :input-class :tw-checkbox))
                 :atts% (atts))))

(deftag (:checkbox-label)
  (:span :class (merge-classes
                 (att :class)
                 (tag-setting :class :tw-checkbox))

         :tabindex (or (att :tabindex) "0")

         (new-children)))

(deftag (:tw-checkbox)
  (:checkbox-box :class (att :class)
                 (when (or (class-exist "checkbox-left" (att :classes))
                           (not (class-exist "checkbox-right" (att :classes))))
                   (:checkbox-check :class (att :check-class)
                                    :input-class (att :check-input-class)))
                 (:checkbox-label :class (att :label-class)
                                  (new-children))
                 (when (class-exist "checkbox-right" (att :classes))
                   (:checkbox-check :class (att :check-class)
                                    :input-class (att :check-input-class)))))

(with-sexp
  (expand
   (with-dom
     (:tw-checkbox :class '("rounded")
                   "Oh Fuck!"))))
