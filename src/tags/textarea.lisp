(in-package :cl-naive-tailwind-kit)

(define-config (:tw-textarea)
  (:config
   (:textarea
    :class '("w-full"
             "px-2"
             "py-2"))))

(deftag (:tw-textarea)
  (:textarea :class (merge-classes
                     (att :class)
                     (tag-setting :class :tw-textarea)
                     (list "w-full"
                           "px-2"
                           "py-2"))

             :atts% (attributes (element))
             (new-children)))

(with-sexp
  (expand
   (with-dom
     (:tw-textarea :class '("rounded")))))
