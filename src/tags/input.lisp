(in-package :cl-naive-tailwind-kit)

(define-config (:tw-input)
  (:config
   (:input-box
    :class '("flex"
             "items-center"
             "w-full"
             "leading-normal"
             "content-center"))

   (:input-sidekick
    :class '("flex"
             "items-center"
             "leading-normal"
             "h-full"))
   (:input-input
    :class '("flex-1"
             "w-full"
             "px-2"
             "leading-normal"
             "bg-transparent"
             "focus:outline-none"))))

(deftag (:input-box)
  (:div :class (merge-classes
                (typecase (att :class)
                  (string (list (concatenate 'string (att :class) " ")))
                  (list (mapcar (lambda (class)
                                  (replace-all class "focus:" "focus-within:"))
                                (att :class))))
                (tag-setting :class :tw-input))

        (new-children)))

(deftag (:input-sidekick)
  (:span :class (merge-classes
                 (att :class)
                 (tag-setting :class :tw.tw-input)
                 (when (or (class-exist "rounded" (att :class))
                           (setting :corners :class))
                   (if (att :left)
                       "rounded-l"
                       "rounded-r")))
         (new-children)))

(deftag (:input-input)
  (:input :class (merge-classes
                  (att :class)
                  (tag-setting :class :tw.tw-input))
          :atts% (attributes (element))))

(deftag (:tw-input)
  (let ((left (find 'left-child (new-children) :key #'tag))
        (right (find 'right-child (new-children) :key #'tag)))
    (:input-box :class (att :class)

                ;; (or
                ;;  (and left (when (class-exist "image-left" (att :class))
                ;;              (:input-sidekick :class (att :left-sidekick-class)
                ;;                               :left "Yes"
                ;;                               left)))
                ;;  (when (class-exist "image-left" (att :class))
                ;;    (:input-sidekick :class (att :left-image-class)
                ;;                     :left "Yes")))
                ;; (:input-input :class (att :input-class)
                ;;               :atts% (attributes (element)))
                ;; (or (and right (when (class-exist "image-right" (att :class))
                ;;                  (:input-sidekick :class (att :right-sidekick-class)
                ;;                                   :right "Yes"
                ;;                                   right)))
                ;;     (when (class-exist "image-right" (att :class))
                ;;       (:input-sidekick :class (att :right-image-class)
                ;;                        :right "Yes")))
                )))

(with-sexp
  (expand
   (with-dom
     (:tw-input :class '("rounded")
                :value "Oh Fuck!"))))
