(in-package :cl-naive-tailwind-kit)

(define-config (:tw-select)
  (:config
   (:select-box
    :class '("flex"
             "items-center"
             "w-full"
             "leading-normal"
             "content-center"))

   (:select-sidekick
    :class '("flex"
             "items-center"
             "p-2"
             "leading-normal"
             "h-full"))
   (:select-input
    :class '(  "flex-1"
             "w-full"
             "leading-normal"
             "focus:outline-none"))))

(deftag (:select-box)
  (:div :select (merge-classes
                 (mapcar (lambda (class)
                           (replace-all class "focus:" "focus-within:"))
                         (att :class))
                 (tag-setting :class :tw-select))

        (new-children)))

(deftag (:select-sidekick)
  (:span :class (merge-classes
                 (att :class)
                 (tag-setting :class :tw-select)
                 (when (or (class-exist "rounded" (att :class))
                           (setting :corners :class))
                   (if (att :left)
                       "rounded-l"
                       "rounded-r")))

         (or (new-children)
             (:div :class "m-1.5 bg-black minion h-em w-em"))))

(deftag (:select-input)
  (:select :class (merge-classes
                   (att :class)
                   (tag-setting :class :tw-select))
           :atts% (attributes (element))
           (new-children)))

(deftag (:tw-select)
  (let ((left (find 'left-child (new-children) :key #'tag))
        (right (find 'right-child (new-children) :key #'tag)))
    (:input-box

     (or
      (and left (when (class-exist "image-left" (att :class))
                  (:select-sidekick :class (att :left-sidekick-class)
                                    :left "Yes"
                                    left)))
      (when (class-exist "image-left" (att :class))
        (:select-sidekick :class (att :left-image-class)
                          :left "Yes")))
     (:select-input :class (att :input-class)
                    :atts% (attributes (element))
                    (reverse (set-difference (new-children) (list left right))))
     (or (and right (when (class-exist "image-right" (att :class))
                      (:input-sidekick :class (att :right-sidekick-class)
                                       :right "Yes"
                                       right)))
         (when (class-exist "image-right" (att :class))
           (:select-sidekick :class (att :right-image-class)
                             :right "Yes"))))))

(deftag (:tw-option)
  (:option :class (merge-classes
                   (att :class)
                   (tag-setting :class :tw-option))

           :atts% (attributes (element))
           (new-children)))

(with-sexp
  (expand
   (with-dom
     (:tw-select :class '( "rounded")
                 (:option :value ""
                          :readonly t
                          :selected t
                          :hidden t
                          "Select an option")
                 (:option "Option A")
                 (:option "Option B")
                 (:option "Option C")))))
