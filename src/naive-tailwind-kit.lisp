(in-package :cl-naive-tailwind-kit)

(defun merge-classes (&rest classes)
  "Merges class names supplied into a single list."

  (remove-duplicates (apply #'append (mapcar 'uiop:ensure-list classes)) :test #'equalp))

(defun clean-classes (classes remove)
  (set-difference classes remove :test #'equalp))

(defparameter *config* nil)

(defun class-exist (class classes &optional value)
  (when (typecase classes
          (string (equalp class classes))
          (list (member class classes :test #'equalp)))
    (or class value)))

(defun scheme-class (classes)
  (or (class-exist "primary" classes :primary)
      (class-exist "secondary" classes :secondary)))

(defun tag-setting (attribute &optional config)
  (select-setting (tag (element))
                  :attribute attribute
                  :config (or config (config (tag (element))))))

(defun setting (selector attribute &optional config)
  (select-setting selector
                  :attribute attribute
                  :config (or config :default)))

(deftag (:h ())
  (let ((tag-name (keywordize (format nil "h~A" (att :size)))))
    (make-instance 'element :tag (tag-convert tag-name)
                            :attributes (attributes (element))
                            :children (new-children))))

(defun replace-all (string part replacement &key (test #'char=))
  "Returns a new string in which all the occurences of the part
is replaced with replacement."
  (when string
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos))))
