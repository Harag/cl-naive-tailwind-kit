(defsystem "cl-naive-tailwind-kit.examples"
  :description ""
  :version "2022.9.16"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-who
               :cl-naive-tailwind-kit
               :cl-naive-dom
               :cl-naive-dom.abt
               :cl-naive-webserver.hunchentoot)
  :components ((:file "examples/package")
               (:file "examples/examples" :depends-on ("examples/package"))
               (:file "examples/alert" :depends-on ("examples/examples"))
               (:file "examples/button" :depends-on ("examples/examples"))
               (:file "examples/input" :depends-on ("examples/examples"))
               (:file "examples/checkbox" :depends-on ("examples/examples"))
               (:file "examples/radio" :depends-on ("examples/examples"))
               (:file "examples/select" :depends-on ("examples/examples"))
               (:file "examples/textarea" :depends-on ("examples/examples"))
               (:file "examples/card" :depends-on ("examples/examples"))))

