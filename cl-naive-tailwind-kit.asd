(defsystem "cl-naive-tailwind-kit"
  :description ""
  :version "2022.9.16"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-dom :cl-naive-dom.abt :cl-naive-dom.js :cl-naive-ccf)
  :components ((:file "src/package")
               (:file "src/naive-tailwind-kit" :depends-on ("src/package"))
               (:file "src/tags/alert" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/button" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/input" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/checkbox" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/radio" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/select" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/textarea" :depends-on ("src/naive-tailwind-kit"))
               (:file "src/tags/card" :depends-on ("src/naive-tailwind-kit"))))

